export interface FunctionDoc {
	name: string, 
	signature: string,
	description: string,
	snippet?: string
}

export interface TableDoc {
	name: string,
	signature: string,
	description: string,
	columns: Array<Dictionary>
}

export interface Dictionary {
  /* eslint-disable-next-line @typescript-eslint/no-explicit-any */
  [keys: string]: any;
}
