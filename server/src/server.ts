import {
	createConnection,
	TextDocuments,
	Diagnostic,
	ProposedFeatures,
	InitializeParams,
	DidChangeConfigurationNotification,
	CompletionItem,
	TextDocumentPositionParams,
	TextDocumentSyncKind,
	InitializeResult,
	Hover
} from 'vscode-languageserver/node';
import { complete, FUNCTIONDOCS, TABLES } from './completion';
import {
	TextDocument
} from 'vscode-languageserver-textdocument';
import * as fs from "fs";
import * as path from 'path';
import type { Dictionary } from './types';

// Create a connection for the server, using Node's IPC as a transport.
// Also include all preview / proposed LSP features.
const connection = createConnection(ProposedFeatures.all);

// Create a simple text document manager.
const documents: TextDocuments<TextDocument> = new TextDocuments(TextDocument);

let hasConfigurationCapability = true;
let hasWorkspaceFolderCapability = false;
// const hasDiagnosticRelatedInformationCapability = false;

connection.onInitialize((params: InitializeParams) => {
  const capabilities = params.capabilities;

  // Does the client support the `workspace/configuration` request?
  // If not, we fall back using global settings.
  hasConfigurationCapability = !!(
    capabilities.workspace && !!capabilities.workspace.configuration
  );
  hasWorkspaceFolderCapability = !!(
    capabilities.workspace && !!capabilities.workspace.workspaceFolders
  );
  // hasDiagnosticRelatedInformationCapability = !!(
  //   capabilities.textDocument &&
  //   capabilities.textDocument.publishDiagnostics &&
  //   capabilities.textDocument.publishDiagnostics.relatedInformation
  // );

  const result: InitializeResult = {
    capabilities: {
      textDocumentSync: TextDocumentSyncKind.Incremental,
      // Tell the client that this server supports code completion.
      completionProvider: {
        resolveProvider: true
      },
      hoverProvider: true
    }
  };
  if (hasWorkspaceFolderCapability) {
    result.capabilities.workspace = {
      workspaceFolders: {
        supported: true
      }
    };
  }
  return result;
});

connection.onInitialized(() => {
  if (hasConfigurationCapability) {
    // Register for all configuration changes.
    connection.client.register(DidChangeConfigurationNotification.type, undefined);
  }
  if (hasWorkspaceFolderCapability) {
    connection.workspace.onDidChangeWorkspaceFolders(() => {
      connection.console.log('Workspace folder change event received.');
    });
  }
});

interface TrinoSettings {
  maxNumberOfProblems: number,
  tablesFilePath: string | undefined
}

// The global settings, used when the `workspace/configuration` request is not supported by the client.
// Please note that this is not the case when using this server with the client provided in this example
// but could happen with other clients.
const defaultSettings: TrinoSettings = { maxNumberOfProblems: 1000, tablesFilePath: undefined };
let globalSettings: TrinoSettings = defaultSettings;

// Cache the settings of all open documents
const documentSettings: Map<string, Thenable<TrinoSettings>> = new Map();

connection.onDidChangeConfiguration(change => {
  if (hasConfigurationCapability) {
    // Reset all cached document settings
    documentSettings.clear();
  } else {
    globalSettings = <TrinoSettings>(
      (change.settings.trinols || defaultSettings)
    );
  }

  // Revalidate all open text documents
  documents.all().forEach(validateTextDocument);
});

function getDocumentSettings(resource: string): Thenable<TrinoSettings> {
  if (!hasConfigurationCapability) {
    return Promise.resolve(globalSettings);
  }
  let result = documentSettings.get(resource);
  if (!result) {
    result = connection.workspace.getConfiguration({
      scopeUri: resource,
      section: 'trinols'
    });
    documentSettings.set(resource, result);
  }
  return result;
}

// Only keep settings for open documents
documents.onDidClose(e => {
  documentSettings.delete(e.document.uri);
});

// The content of a text document has changed. This event is emitted
// when the text document first opened or when its content has changed.
documents.onDidChangeContent(change => {
  validateTextDocument(change.document);
});

async function validateTextDocument(textDocument: TextDocument): Promise<void> {
  // In this simple example we get the settings for every validate run.
  // const settings = await getDocumentSettings(textDocument.uri);

  // The validator creates diagnostics for all uppercase words length 2 and more
  // const text = textDocument.getText();
  // const pattern = /\b[a-z]{2,}\b/g;
  // let m: RegExpExecArray | null;

  // let problems = 0;
  const diagnostics: Diagnostic[] = [];
  // while ((m = pattern.exec(text)) && problems < settings.maxNumberOfProblems) {
  // 	problems++;
  // 	const diagnostic: Diagnostic = {
  // 		severity: DiagnosticSeverity.Warning,
  // 		range: {
  // 			start: textDocument.positionAt(m.index),
  // 			end: textDocument.positionAt(m.index + m[0].length)
  // 		},
  // 		message: `${m[0]} is all uppercase.`,
  // 		source: 'ex'
  // 	};
  // 	if (hasDiagnosticRelatedInformationCapability) {
  // 		diagnostic.relatedInformation = [
  // 			{
  // 				location: {
  // 					uri: textDocument.uri,
  // 					range: Object.assign({}, diagnostic.range)
  // 				},
  // 				message: 'Spelling matters'
  // 			},
  // 			{
  // 				location: {
  // 					uri: textDocument.uri,
  // 					range: Object.assign({}, diagnostic.range)
  // 				},
  // 				message: 'Particularly for names'
  // 			}
  // 		];
  // 	}
  // 	diagnostics.push(diagnostic);
  // }

  // Send the computed diagnostics to VSCode.
  connection.sendDiagnostics({ uri: textDocument.uri, diagnostics });
}

connection.onDidChangeWatchedFiles(_change => {
  // Monitored files have change in VSCode
  connection.console.log('We received an file change event');
  console.log(_change);
});

// async function getSettings(_textDocumentPosition: TextDocumentPositionParams): Promise<TrinoSettings> {
//   return await getDocumentSettings(_textDocumentPosition.textDocument.uri);
// }

/**
 * Return file content for runtimeSettings.json if file exists.
 */
function getRuntimeSettings(): Dictionary | undefined {
  const settingsPath = path.join(
    path.resolve(__dirname), "..", "..", "runtimeSettings.json"
  );
  if (!fs.existsSync(settingsPath)) {
    return undefined;
  }
  const jsonData = fs.readFileSync(settingsPath); 
  const d = JSON.parse(jsonData.toString());
  return d;
}

connection.onCompletion((_textDocumentPosition: TextDocumentPositionParams): PromiseLike<CompletionItem[]> => {
  // The pass parameter contains the position of the text document in
  // which code complete got requested. For the example we ignore this
  // info and always provide the same completion items.
  return getDocumentSettings(_textDocumentPosition.textDocument.uri).then((result) => {
  const position = { line: _textDocumentPosition.position.line, column: _textDocumentPosition.position.character };
  const documentText = documents.get(_textDocumentPosition.textDocument.uri)?.getText();
  return connection.workspace.getWorkspaceFolders().then((folders) => {
    const runtimeSettings = getRuntimeSettings();
    if (folders?.length == 0 || !folders || !runtimeSettings) {
      // use tablesFilePath from setting.json
      return complete(position, result.tablesFilePath, documentText);
    }
    const workspaceFolder = path.resolve(folders[0].uri.replace("file://", ""));
    const folderSetting = runtimeSettings[workspaceFolder];
    if (!folderSetting) {
      // Workspace folder haven't been set yet. This may take a few seconds.
      return complete(position, undefined, documentText);
    }
    const tableFilePath = runtimeSettings[workspaceFolder].tableFilePath;
    return complete(position, tableFilePath, documentText);
  });
  });
});

// This handler resolves additional information for the item selected in
// the completion list.
connection.onCompletionResolve(
  (item: CompletionItem): CompletionItem => {
    return item;
  }
);

/**
 * Return the word the user hovered without spaces or special
 * characters.
 *
 * @param text - the text found the user hover.
 * @param index - the index of the hover.
 * @returns the cleaned up word hovered.
 */
function getWord(text: string, index: number) {
  const first = text.lastIndexOf(' ', index);
  const last = text.indexOf(' ', index);
  return text
    .substring(first !== -1 ? first : 0, last !== -1 ? last : text.length - 1)
    .split("(")[0]
    .replace(/[\W]+/g,"")
    .trim();
}

/**
 * Search and return the over information if the word hovered is a table.
 *
 * @param tableName - the table name to search for hover info.
 * @returns the hover information if table is found.
 */
function getTableHover(tableName: string): Hover | undefined {
  if (!TABLES) {
    return undefined;
  }
  const tableDetails = TABLES.find(table => table.name == tableName);
    if (tableDetails) {
      return {
        contents: {
          kind: 'markdown',
          value: `${tableDetails.signature}\n\n${tableDetails.description}`,
        },
      };
    }
  return undefined;
}

connection.onHover(({ textDocument, position }): Hover | undefined => {
  const document = documents.get(textDocument.uri);

  const start = {
    line: position.line,
    character: 0,
  };
  const end = {
    line: position.line + 1,
    character: 0,
  };
  if (document == undefined) {
    return undefined;
  }
  const text = document.getText({ start, end });
  const index = document.offsetAt(position) - document.offsetAt(start);
  const word = getWord(text, index);
  if (word == '') {
    return undefined;
  }

  const functionDetails = FUNCTIONDOCS.find(func => func.name == word);
  if (functionDetails == undefined) {
    return getTableHover(word);
  }
  return {
    contents: {
      kind: 'markdown',
      value: `${functionDetails.signature}\n\n${functionDetails.description}`,
    },
  };
});

// Make the text document manager listen on the connection
// for open, change and close text document events
documents.listen(connection);

// Listen on the connection
connection.listen();
