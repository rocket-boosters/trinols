import {
	CompletionItemKind,
	CompletionItem,
	MarkupKind
} from 'vscode-languageserver/node';
import { InsertTextFormat } from "vscode-languageserver-types";
import { getText, resourceFolder } from "./filer";
import * as path from 'path';
import { existsSync } from 'fs';
import type { Dictionary, FunctionDoc, TableDoc } from './types';

export const FUNCTIONDOCS: FunctionDoc[] = getResourceFile("functions.json").functions;
export const KEYWORDS: string[] = getResourceFile("keywords.json").keywords;
export const TYPES: string[] = getResourceFile("types.json").types;
export let TABLES: (TableDoc[] | undefined) = undefined;
const SORTFIRST: string[] = ["FROM", "ORDER BY", "GROUP BY"];

function getResourceFile(filename: string): Dictionary {
	const filePath = path.join(resourceFolder(), filename);
	return (getText(filePath, "json") as Dictionary);
}

function getTables(tablesFilePath: (string | undefined)): (TableDoc[] | undefined) {
  if (tablesFilePath == undefined || tablesFilePath == "") {
    return undefined;
  }
  if (!existsSync(path.join(tablesFilePath))) {
    return undefined;
  }
  const text = getText(tablesFilePath);
  return text.tables;
}

/**
 * Returns a TableDoc list for tables that are in the documentText. This will be used
 * to reduce the amount of complitions shown when typing.
 * 
 * @param tables A list of all the tables to check if they exist.
 * @param documentText The text to search for the tables.
 */
function getTablesInDocument(tables: TableDoc[], documentText: string): TableDoc[] {
	return tables.filter(table => documentText.search(table.name) > -1);
}

const addTrinoDocsLink = (description: string, label: string) => {
	return `${description}\n\n[TRINO LINK](https://trino.io/docs/current/search.html?q=${label})`
}

/**
 * Returns the completion items for tables in the document. A combination of table.column and
 * columns will be generated.
 * 
 * @param tablesInDocument Tables that were identified in the document.
 */
function getCompletionsForTablesInDocument(tablesInDocument: TableDoc[]) {
	const tableColumns = tablesInDocument.map(table => {
		const tableInDocumentColumns: CompletionItem[] = table.columns.map(column => ({
			label: `${table.name}.${column.name}`,
			kind: CompletionItemKind.Field,
			detail: column.signature,
			documentation: { kind: MarkupKind.Markdown, value: column.description }
		}));
		const columnsInDocumentColumns: CompletionItem[] = table.columns.map(column => ({
			label: column.name,
			kind: CompletionItemKind.Field,
			detail: column.signature,
			documentation: { kind: MarkupKind.Markdown, value: column.description }
		}));
		tableInDocumentColumns.push(...columnsInDocumentColumns);
		return tableInDocumentColumns;
	});
	return tableColumns;
}

/**
 * Sort values first if values is in SORTFIRST list.
 *
 * @param key - The key to be sorted
 * @returns - The sort value or undefined if not supposed to sort first.
 */
function sortFirst(key: string): string | undefined {
  if (SORTFIRST.includes(key)) {
    return "0";
  }
  return undefined;
}

export function complete(position: Record<string, unknown>, tablesFilePath: (string | undefined), documentText: (string | undefined)): CompletionItem[] {
  TABLES = getTables(tablesFilePath);
  const keywords: CompletionItem[] = (
    KEYWORDS.map(key => ({ label: key, kind: CompletionItemKind.Keyword, sortText: sortFirst(key) }))
  );
  const variables: CompletionItem[] = (
    TYPES.map(key => ({ label: key, kind: CompletionItemKind.TypeParameter }))
  );

  const functions: CompletionItem[] = FUNCTIONDOCS.map(func => ({
    label: func.name,
    kind: CompletionItemKind.Function,
    detail: func.signature,
    insertText: func.snippet ? func.snippet : func.name,
    insertTextFormat: func.snippet ? InsertTextFormat.Snippet : InsertTextFormat.PlainText,
    documentation: { kind: MarkupKind.Markdown, value: addTrinoDocsLink(func.description, func.name) }
  }));

  const items = keywords;
  items.push(...variables);
  items.push(...functions);

  const aliasInFile: Array<Dictionary> = [];
  if (documentText && TABLES !== undefined) {
    const pattern = /\w+\s+(as|AS)\s+\w+/g;
    let m: RegExpExecArray | null;
    while ((m = pattern.exec(documentText))) {
      const possibleTable = m[0];
      const tableName = possibleTable.split(' ')[0].trim();
      const alias = possibleTable.split(' ')[possibleTable.split(' ').length - 1].trim();
      const aliasTable = { tableName, alias };
      const actialTableMatch = TABLES.filter(table => table.name == tableName);
      if (actialTableMatch.length > 0) {
        aliasInFile.push(aliasTable);
      }
    }
  }

  if (TABLES !== undefined) {
    const aliasTables: TableDoc[] = [];
    aliasInFile.forEach(alias => {
      if (TABLES !== undefined) {
        const actualTable: (TableDoc[] | []) = TABLES.filter(t => t.name == alias.tableName);
        if (actualTable.length > 0) {
          const constFoundTable: TableDoc = actualTable[0];
          aliasTables.push({
            name: alias.alias,
            signature: constFoundTable.signature,
            description: constFoundTable.description,
            columns: constFoundTable.columns
          });
        }
      }
    });
    if (aliasTables.length > 0) {
      TABLES.push(...aliasTables);
    }

    const tablesDetails: CompletionItem[] = TABLES.map(table => ({
      label: table.name,
      kind: CompletionItemKind.Event,
      detail: table.signature,
      documentation: { kind: MarkupKind.Markdown, value: table.description }
    }));
    items.push(...tablesDetails);

    const tablesInDocument = getTablesInDocument(TABLES, documentText || '');
    const tableColumns: CompletionItem[][] = getCompletionsForTablesInDocument(tablesInDocument);
    const columns: CompletionItem[] = [];
    tableColumns.forEach(tableColumns => columns.push(...tableColumns));
    items.push(...columns);
  }
  return items;
}