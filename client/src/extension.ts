
import * as path from 'path';
import * as fs from "fs";
import OpenAI from 'openai';
import { getResourceFile } from './filer';

import { workspace, ExtensionContext, commands, window, Range } from 'vscode';

import {
  LanguageClient,
  LanguageClientOptions,
  ServerOptions,
  TransportKind
} from 'vscode-languageclient/node';
import { format } from "sql-formatter";

let client: LanguageClient;

interface Dictionary {
  /* eslint-disable-next-line @typescript-eslint/no-explicit-any */
  [keys: string]: any;
}

interface Column {
  name: string
}
interface Table {
  name: string,
  columns: Column[]
}

const toLowerFunctions = (line: string, functions: string[]): string => {
  let newLine = line;
  functions.forEach((f: string) => {
    if (line.search(f) > -1) {
      newLine = (
        newLine
        .replace(`${f}(`, `${f.toLocaleLowerCase()}(`)
        .replace(" DAY ", " day ")
      )
    }
  })
  return newLine;
}


const _format = (query: string): string => {
	const sqlFormatterQuery = (
    format(query, {
      language: "trino",
      tabWidth: 2,
      logicalOperatorNewline: "before",
      keywordCase: "upper",
    })
  );
  const keywords = getResourceFile("keywords.json").keywords;
  const funcionsNames = (
    getResourceFile("functions.json").functions
    .map((f: Dictionary) => f.name.toUpperCase())
    .filter((n: string) => !keywords.includes(n))
  )

  const onFormattedQuery = (
    sqlFormatterQuery.toString().split("\n")
    .map((line: string) => {
      if (line.includes(" ON ")) {
        const beginSpacesCount = line.search(/\S/);
        const newOnLone = `\n${"".padStart(2 + beginSpacesCount, " ")}ON `;
        return line.replace(" ON ", newOnLone);
      } else {
        return line;
      }
    })
    .map((line: string) => (toLowerFunctions(line, funcionsNames)))
  )
  const formattedQuery = onFormattedQuery.join("\n");
	return formattedQuery;
}

function formartQuery(context: ExtensionContext, query: string) {
  const lineNumber: number = window.activeTextEditor.document.lineCount;
  query = window.activeTextEditor.document.getText();
  const formattedQuery = _format(query)
  window.showInformationMessage("Formatted query");
  window.activeTextEditor.edit(editorBuilder => { editorBuilder.replace(new Range(0,0,lineNumber + 1,0), formattedQuery); });
}

/**
 * Return file content if file exists.
 * @param filePath - the file path to get the content.
 */
function getJsonContent(filePath: string): Dictionary {
  if (!fs.existsSync(filePath)) {
    return {};
  }
  const jsonData = fs.readFileSync(filePath); 
  const d = JSON.parse(jsonData.toString());
  return d;
}

/**
 * Add the path to runtimeSettings.json for the current workspace folder.
 * This will be used as a form of comunication with the client server where
 * it needs to know the tableFilePath based on workspaces.
 * @param context - the extension context
 * @param path - the path to add to the runtimeSettings.js
 */
async function setTrinolsTablesFilePath(context: ExtensionContext, path: string): Promise<void> {
  const extensionPath = context.asAbsolutePath("runtimeSettings.json");
  const settings = getJsonContent(extensionPath);
  const workspaceFolders = workspace.workspaceFolders;
  if (!workspaceFolders) {
    return;
  }
  const workspacePath = workspace.workspaceFolders[0].uri.path;
  const tableFilePath = path || "";
  settings[workspacePath] = { tableFilePath };
  const settingsJson = JSON.stringify(settings, null, 2);
  fs.writeFile(extensionPath, settingsJson, 'utf8', function (err) {
    if (err) {
        console.log("An error occured while writing JSON Object to File.");
        return console.log(err);
    }
  });
}

const prepSqlPrefix = (tables: Table[], type: "query" | "question"): string => {
  const tableAndColumns = tables.map((t: Table) => {
    const tableName = t.name;
    const columns = t.columns.map((c: Column) => (c.name))
    return `${tableName}(${columns})\n`
  })
  const tableText = tableAndColumns.join("# ");
  if (type == "query") {
    return `### TrinoSQL tables, with their properties\n#\n# ${tableText}\n#\n### A TrinoSQL query for `;
  } else {
    return `### TrinoSQL tables, with their properties\n#\n# ${tableText}\n#\n### Where to find `;
  }
}

/**
 * Ask OpenAI a question.
 *
 * @param token - OpenAI token.
 * @param question The question to ask OpenAI.
 * @returns The OpenAI answer.
 */
async function askOpenAi(token: string, model: string, question: string) {
  const opanai = new OpenAI({
    apiKey: token,
  });

  const response = await opanai.chat.completions.create({
    model: model,
    messages: [{ role: "user", content: question }],
    temperature: 0.7,
    max_tokens: 250,
    top_p: 1,
    frequency_penalty: 0,
    presence_penalty: 0
  });
  return response.choices[0].message.content;
}

const tablePath = (context: ExtensionContext) => {
  const extensionPath = context.asAbsolutePath("runtimeSettings.json");
  const settings = getJsonContent(extensionPath);
  const workspaceFolders = workspace.workspaceFolders;
  if (!workspaceFolders) {
    return;
  }
  const workspacePath = workspace.workspaceFolders[0].uri.path;
  return getJsonContent(settings[workspacePath].tableFilePath);
}

/**
 * Execute schema bot command.
 *
 *  For this to work you will need a OpenAI that you can get here:
 *      https://beta.openai.com/account/api-keys

 *  This command camber be helpful when you are trying to know where information can
 *  be found in your schemas. It is set up to return a sql query, but keep in mind this
 *  is just to be used as a starter query.
 * 
 *  Here are some example of questions you can ask:
 * Where can I find user roles?
 *  * owner_id equal user.id, how to I get the owner and their email for account 123.
 *
 *  This last one would return something like:
 *      SELECT
 *        accounts.owner_id,
 *        user.user_email
 *      FROM
 *        accounts
 *      JOIN user
 *        ON accounts.owner_id = user.id
 *      WHERE
 *        accounts.key = 123;
 */
async function querySampleQuestion(context: ExtensionContext, type: "query" | "question"): Promise<void> {
  const question = await window.showInputBox();
  const trinolsConfig = workspace.getConfiguration("trinols")
  const openAiToken  = trinolsConfig.get("openAiToken");
	const model = trinolsConfig.get("openAiModel") || "gpt-4o";
  const tableFile = tablePath(context);
  const prefix = prepSqlPrefix(tableFile.tables, type);
  const fullQuestion = `${prefix} ${question}`;
  const answer = await askOpenAi(openAiToken.toString(), model.toString(), fullQuestion)
  const textLength: number = window.activeTextEditor.document.lineCount;
  if (type == "query") {
		const formattedAnswer = _format(answer);
    window.activeTextEditor.edit(editorBuilder => { editorBuilder.replace(new Range(textLength,0, textLength, 0), `\n\n${formattedAnswer}`); });
  } else {
    window.showInformationMessage(answer, { modal: true });
  }
}

function activatePublicCommands(context: ExtensionContext) {
  context.subscriptions.push(
    commands.registerCommand(
      "trinols.formatQuery", (context: ExtensionContext, query: string) => formartQuery(context, query)
    )
  );
  context.subscriptions.push(
    commands.registerCommand(
      "trinols.setTrinolsTablesFilePath",
      (path: string) => setTrinolsTablesFilePath(context, path)
    )
  );
  context.subscriptions.push(
    commands.registerCommand(
      "trinols.schemaQuestion",
      () => querySampleQuestion(context, "question")
    )
  );
  context.subscriptions.push(
    commands.registerCommand(
      "trinols.querySample",
      () => querySampleQuestion(context, "query")
    )
  );
}

export function activate(context: ExtensionContext): void {
  activatePublicCommands(context);
  const serverModule = context.asAbsolutePath(
    path.join('server', 'out', 'server.js')
  );
  // The debug options for the server
  // --inspect=6009: runs the server in Node's Inspector mode so VS Code can attach to the server for debugging
  const debugOptions = { execArgv: ['--nolazy', '--inspect=6009'] };

  // If the extension is launched in debug mode then the debug server options are used
  // Otherwise the run options are used
  const serverOptions: ServerOptions = {
    run: { module: serverModule, transport: TransportKind.ipc },
    debug: {
      module: serverModule,
      transport: TransportKind.ipc,
      options: debugOptions
    }
  };

  // Options to control the language client
  const clientOptions: LanguageClientOptions = {
    // Register the server for plain text documents
    documentSelector: [{ scheme: 'file', language: 'sql' }],
    synchronize: {
      // Notify the server about file changes to '.clientrc files contained in the workspace
      fileEvents: workspace.createFileSystemWatcher('**/.clientrc')
    }
  };

  // Create the language client and start the client.
  client = new LanguageClient(
    'trinols',
    'Language Server trinols',
    serverOptions,
    clientOptions
  );
  client.onReady().then(() => {
    client.onNotification("trinols/log", (logData: Dictionary) => {
        console.log(logData);
    });
});
  // Start the client. This will also launch the server
  client.start();
}

export function deactivate(): Thenable<void> | undefined {
  if (!client) {
    return undefined;
  }
  return client.stop();
}
