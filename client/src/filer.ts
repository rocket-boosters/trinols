import * as fs from "fs";
import * as yaml from "js-yaml";
import * as path from 'path';

export interface Dictionary {
  /* eslint-disable-next-line @typescript-eslint/no-explicit-any */
  [keys: string]: any;
}

export function resourceFolder(): string {
  return path.join(__dirname, "..", "..", "resources");
}

/**
 * Get data from the file.
 * Examples to use: 	
 *   const text = getText(path.join(resourceFolder(), 'data.yaml'), "dictionary");
 *   window.showInformationMessage(text.test.test2);
 * @param filePath - The path for the file.
 * @param source - the source file extension for the the data. json, txt, or yaml. 
 * @returns 
 */
export function getText(filePath: string, source = "json"): Dictionary {
  const stringData = fs.readFileSync(filePath, 'utf8');
  if (source == 'string') {
    return {text : stringData} ;
  }
  if (source == 'yaml') {
    const yamlData = (yaml.load(stringData) as Dictionary);
    return {text: yamlData};
  }
  if (source == "json") {
    const jsonData = fs.readFileSync(filePath); 
    const d = JSON.parse(jsonData.toString());
    return d;
  }
  return {};
}

export function getResourceFile(filename: string): Dictionary {
  const filePath = path.join(resourceFolder(), filename);
  return (getText(filePath, "json") as Dictionary);
}