"""Entry point for trinols cli."""

from trinols._downloader import run as update  # noqa: F401
from trinols._parser import parse


COMMANDS = {"update": update}


def main():
    """Entry point for library CLI."""
    # TODO
    args = parse()
    COMMANDS[args["command"]](args)


if __name__ == "__main__":  # pragma: no cover
    main()
