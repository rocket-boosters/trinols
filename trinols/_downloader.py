"""Contains logic for downloading trino documentation."""

import json
import pathlib
import re
import time
import typing

# Adds chromedriver binary to path
import chromedriver_binary  # noqa
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common import by

from trinols import _post_mods
from trinols import configs

NAME_REGEX = re.compile(r"(^[^(\n]*)\w")
URL_REGEX = re.compile(r"(https?:\/\/?)")

DEFAULT_SNIPPET_FORMAT = "${index}"
SNIPPET_FORMAT_OPTIONS = {
    "unit": "${{{index}|'millisecond','second','minute','hour','day','week','month',"
    "'quarter','year'|}} "
}

OPTIONS = Options()
OPTIONS.add_argument("--window-size=1920,1200")
OPTIONS.add_argument("--headless")
OPTIONS.add_argument("--no-sandbox")
OPTIONS.add_argument("--disable-dev-shm-usage")


DRIVER_PATH = pathlib.Path(__file__).parent.joinpath("chromedriver")

NOT_TO_LOOK_URLS = [
    "https://trino.io/docs/current/functions/list.html",  # repeating stuff
    "https://trino.io/docs/current/functions/list-by-topic.html",  # repeating stuff
    "https://trino.io/docs/current/language.html",
    "https://trino.io/docs/current/sql.html",
]

# Need to handle this in a better way.
BAD_TYPES = [
    "Boolean",
    "Integer",
    "Floating-point",
    "Fixed-precision",
    "Date and time",
    "Structural",
    "Network address",
    "Quantile digest",
    "Trino type support and mapping",
    "Exact numeric",
]


def is_url(wanna_be_url: typing.Any) -> bool:
    """
    Return True if value is a url.

    :param wanna_be_url:
        The possible string that could be a url.
    """
    if not isinstance(wanna_be_url, str):
        return False
    if wanna_be_url.startswith("https:"):
        return True
    return False


def find_el_by_text(driver: typing.Any, text: str) -> list:
    """
    Find elements in the selenium driver based on text.

    :param driver:
        The selenium driver.
    :param text:
        The text to search elements with.
    """
    return driver.find_elements(by.By.XPATH, (f"//*[contains(text(),'{text}')]"))


def _get_func_elements(tf):
    try:
        signature = "\n\n".join(tf.get_attribute("innerText").split("\n\n")[:1])
        sql_replacers = []
        for sql in tf.find_elements(by=by.By.CLASS_NAME, value="highlight-sql"):
            previous_sql_text = sql.get_attribute("innerText")
            new_sql_text = f"```sql\n{previous_sql_text}\n```"
            sql_replacers.append((previous_sql_text, new_sql_text))
        description = tf.get_attribute("innerText")
        for previous, new in sql_replacers:
            description: str = description.replace(previous, new)
        description = "\n\n".join(description.split("\n\n")[1:])
        description = f"{description}\n\n"
    except ValueError:
        raise
        pass
    name = NAME_REGEX.match(tf.text).group()
    params_match = re.search(r"\((.+)\)\s", signature)
    snippet = None
    if params_match:
        params = [
            SNIPPET_FORMAT_OPTIONS.get(variable, DEFAULT_SNIPPET_FORMAT).format(
                index=str(i)
            )
            for i, variable in list(enumerate(params_match.group(1).split(","), 1))
        ]
        snippet = f'{name}({", ".join(params)})$0'
    return {
        "name": name,
        "signature": signature,
        "description": description,
        "snippet": snippet,
    }


def _get_function_docs(function_url: str):
    """
    Download functions documentation form the url provided.

    :param function_url:
        The function url from configs.FUNCTION_DOCS_URLS
    """
    driver = webdriver.Chrome(options=OPTIONS)
    try:
        driver.get(function_url)
        functions: typing.List[typing.Dict[str, str]] = []
        for tf in driver.find_elements(by=by.By.CLASS_NAME, value="function"):
            functions.append(_get_func_elements(tf))
        for tf in driver.find_elements(by=by.By.CLASS_NAME, value="data"):
            functions.append(_get_func_elements(tf))
        time.sleep(2)
        return functions
    finally:
        driver.quit()


def download_functions():
    """Download trino data types documentation."""
    functions = []
    for function_url in configs.FUNCTION_DOCS_URLS:
        functions.extend(_get_function_docs(function_url))
    with open(configs.RESOURCES_PATH.joinpath("functions.json"), "w") as f:
        f.write(json.dumps({"functions": functions}, indent=4))


def download_reserved_keywords():
    """Download trino data keywords documentation."""
    driver = webdriver.Chrome(options=OPTIONS)
    try:
        driver.get(configs.RESERVED_WORDS_URL)
        keywords: typing.List[typing.Dict[str, str]] = []
        table = driver.find_elements(by=by.By.CLASS_NAME, value="md-typeset__table")
        for reserved in (
            table[0]
            .find_element(by=by.By.TAG_NAME, value="table")
            .find_elements(by=by.By.CLASS_NAME, value="docutils")
        ):
            word = reserved.get_attribute("innerText")
            if word == "true":
                break
            keywords.append(word)
        with open(configs.RESOURCES_PATH.joinpath("keywords.json"), "w") as f:
            f.write(json.dumps({"keywords": keywords}, indent=4))
    finally:
        driver.quit()


def download_data_types():
    """Download trino data types documentation."""
    driver = webdriver.Chrome(options=OPTIONS)
    try:
        driver.get(configs.DATA_TYPES_URL)
        types: typing.List[typing.Dict[str, str]] = []
        data_types = driver.find_elements(
            by=by.By.CLASS_NAME, value="md-nav--secondary"
        )
        for reserved in data_types:
            for i in reserved.find_elements(by=by.By.TAG_NAME, value="a"):
                _type = i.get_attribute("innerText")
                if _type not in types and _type not in BAD_TYPES:
                    types.append(_type)
        with open(configs.RESOURCES_PATH.joinpath("types.json"), "w") as f:
            f.write(json.dumps({"types": types}, indent=4))
    finally:
        driver.quit()


def run(args: dict):
    """
    Download all the trino documentation to resources folder.

    This will download function, keywords, data types and other information.
    """
    download_functions()
    # TODO: keywords and types are not downloading docs. They will autocomplete though.
    download_reserved_keywords()
    download_data_types()

    _post_mods.apply_snippet_mods()
    _post_mods.apply_keyword_mods()
