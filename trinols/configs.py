"""Holds common configuration used in trinols."""

import pathlib
import typing

FUNCTION_DOCS_URLS: typing.List[str] = [
    # 'https://trino.io/docs/current/language.html',
    # 'https://trino.io/docs/current/sql.html',
    "https://trino.io/docs/current/functions/datetime.html",
    "https://trino.io/docs/current/functions/aggregate.html",
    "https://trino.io/docs/current/functions/array.html",
    "https://trino.io/docs/current/functions/binary.html",
    "https://trino.io/docs/current/functions/bitwise.html",
    "https://trino.io/docs/current/functions/color.html",
    "https://trino.io/docs/current/functions/comparison.html",
    "https://trino.io/docs/current/functions/conditional.html",
    "https://trino.io/docs/current/functions/conversion.html",
    "https://trino.io/docs/current/functions/decimal.html",
    "https://trino.io/docs/current/functions/geospatial.html",
    "https://trino.io/docs/current/functions/hyperloglog.html",
    "https://trino.io/docs/current/functions/ipaddress.html",
    "https://trino.io/docs/current/functions/json.html",
    "https://trino.io/docs/current/functions/lambda.html",
    "https://trino.io/docs/current/functions/logical.html",
    "https://trino.io/docs/current/functions/ml.html",
    "https://trino.io/docs/current/functions/map.html",
    "https://trino.io/docs/current/functions/math.html",
    "https://trino.io/docs/current/functions/qdigest.html",
    "https://trino.io/docs/current/functions/regexp.html",
    "https://trino.io/docs/current/functions/session.html",
    "https://trino.io/docs/current/functions/setdigest.html",
    "https://trino.io/docs/current/functions/string.html",
    "https://trino.io/docs/current/functions/system.html",
    "https://trino.io/docs/current/functions/teradata.html",
    "https://trino.io/docs/current/functions/tdigest.html",
    "https://trino.io/docs/current/functions/url.html",
    "https://trino.io/docs/current/functions/uuid.html",
    "https://trino.io/docs/current/functions/window.html",
]
RESERVED_WORDS_URL = "https://trino.io/docs/current/language/reserved.html"
DATA_TYPES_URL = "https://trino.io/docs/current/language/types.html"

RESOURCES_PATH = pathlib.Path(__file__).parent.absolute().joinpath("..", "resources")
