import argparse
import typing


def parse() -> typing.Dict:
    """Populate the parser for the user command."""
    parser = argparse.ArgumentParser(description="CLI tool for trinols.")
    subparsers = parser.add_subparsers(dest="command")
    subparsers.add_parser(name="update", help="Download new docs.")
    return vars(parser.parse_args())
