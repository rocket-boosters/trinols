import json
import typing

from trinols import configs

KEYWORD_MODS: typing.Dict[str, str] = {}

ADD_KEYWORDS = ["GROUP BY", "ORDER BY", "LIMIT", "INTERVAL"]


def apply_keyword_mods():
    """
    Apply keyword mods to the keywords.json resource.

    This will add or modify downloaded keywords.
    """
    with open(configs.RESOURCES_PATH.joinpath("keywords.json"), "r") as f:
        content = json.loads(f.read())
    with open(configs.RESOURCES_PATH.joinpath("keywords.json"), "w") as f:
        mod_keywords = [KEYWORD_MODS.get(k, k) for k in content["keywords"]]
        mod_keywords.extend(ADD_KEYWORDS)
        print(json.dumps(mod_keywords, indent=4))
        f.write(json.dumps({"keywords": mod_keywords}, indent=4))
