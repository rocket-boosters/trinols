import json
import typing

from trinols import configs


class _Function(typing.TypedDict):
    """Typed dict for function."""

    name: str
    signature: str
    description: str
    snippet: str


class _SnippetMod(typing.TypedDict):
    """Typed dict for snipped mods."""

    name: str
    snippet: str


SNIPPETS_MODS: typing.List[_SnippetMod] = [
    {"name": "json_extract_scalar", "snippet": "json_extract_scalar($1, ${2:'$.'})$0"},
    {"name": "json_extract", "snippet": "json_extract($1, ${2:'$.'})$0"},
]


def _get_new_function(function: _Function) -> typing.Optional[_Function]:
    """Return the same function with a new snippet if any is found."""
    mods_finder: typing.Optional[_SnippetMod] = next(
        (m for m in SNIPPETS_MODS if m["name"] == function["name"]), None
    )
    if to_be_modified := mods_finder:
        new_function = function.copy()
        new_function.update({"snippet": to_be_modified["snippet"]})
        return new_function
    else:
        return function


def apply_snippet_mods():
    """Apply snippet mods to the functions."""
    with open(configs.RESOURCES_PATH.joinpath("functions.json"), "r") as f:
        content = json.loads(f.read())
    with open(configs.RESOURCES_PATH.joinpath("functions.json"), "w") as f:
        mod_functions = [_get_new_function(f) for f in content["functions"]]
        print(json.dumps(mod_functions, indent=4))
        f.write(json.dumps({"functions": mod_functions}, indent=4))
