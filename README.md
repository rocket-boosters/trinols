# trinols

[![coverage report](https://gitlab.com/rocket-boosters/trinols/badges/main/coverage.svg)](https://gitlab.com/rocket-boosters/trinols/commits/main)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Code style: flake8](https://img.shields.io/badge/code%20style-flake8-white)](https://gitlab.com/pycqa/flake8)
[![Code style: mypy](https://img.shields.io/badge/code%20style-mypy-white)](http://mypy-lang.org/)


Language server extension for [trino](https://trino.io) (previously known as presto).

`trinols` will let you have trino docs at your fingertips.

## Functionality

This Language Server works for sql files (.sql). It has the following language features:
- `Completions`: with documentation from [trino](http://trino.io).
- `Hover`: hover over function names to find their definition.
- `trinols: Format`: Format your query. Menu option and palette.
- [OpenAI Features](https://beta.openai.com/account/api-keys): you will need an api token
  from OpenAI.com and also have your `Table File Path`(see below).
  - `trinols: Query Sample`: Uses OpenAI powerful AI to build a query based on
  you detailed request. Returns a sample query based on your schema.
    - This will give you a sample query. You can be very descriptive. See the 
      Feature Details section for more information.
  - `trinols: Schema Question`: Answer questions like: Where can I find email for
  users?
    - This tells you what tables to find that information. You can be very descriptive. See the 
      Feature Details section for more information.
- TODO: Diagnostics regenerated on each file change or configuration change.
- TODO: `trinols: Doc Search`: This command opens a screen that allows you to search all trinols documentation and also tables.json (see Table File Path below.).

## Settings

### OpenAI Api Key

In the `Preference -> Settings -> User -> trino: Open Ai Token`. This
token will be used for feature like `trinols: Schema Question`,
and `trinols: Query Sample`.

### Table File Path

`trinols` has support for table information autocomplete. You will need to provide a
tables.json file containing the information needed. Change this setting in trinols settings
in vscode (`>> preferences > settings > trinols: Tables File Path`). For more details see
`Feature Details -> Table File Path`.

## Structure

```
.
├── client // Language Client
│   ├── src
│   │   ├── test // End to End tests for Language Client / Server
│   │   └── extension.ts // Language Client entry point
├── package.json // The extension manifest.
└── server // Language Server
    └── src
        └── server.ts // Language Server entry point
```

## Running the Sample

- Run `npm install` in this folder. This installs all necessary npm modules in both the client and server folder
- Open VS Code on this folder.
- Press Ctrl+Shift+B to start compiling the client and server in [watch mode](https://code.visualstudio.com/docs/editor/tasks#:~:text=The%20first%20entry%20executes,the%20HelloWorld.js%20file.).
- Switch to the Run and Debug View in the Sidebar (Ctrl+Shift+D).
- Select `Launch Client` from the drop down (if it is not already).
- Press ▷ to run the launch config (F5).
- If you want to debug the server as well, use the launch configuration `Attach to Server`
- In the [Extension Development Host](https://code.visualstudio.com/api/get-started/your-first-extension#:~:text=Then%2C%20inside%20the%20editor%2C%20press%20F5.%20This%20will%20compile%20and%20run%20the%20extension%20in%20a%20new%20Extension%20Development%20Host%20window.) instance of VSCode, open a document in 'plain text' language mode.
  - Type `j` or `t` to see `Javascript` and `TypeScript` completion.
  - Enter text content such as `AAA aaa BBB`. The extension will emit diagnostics for all words in all-uppercase.
5

## Feature Details

### trinols: Query Sample

Uses OpenAI powerful AI to build a query based on you detailed request. For this to work you 
will need a OpenAI that you can get here: https://beta.openai.com/account/api-keys

This command is helpful when you are trying to know where information can be found in your
schemas/tables. It is set up to return a sql query, but keep in mind this is just to be used
as a starter query.

Here are some example of questions you can ask:
* Where can I find user roles?
* owner_id equal user.id, how to I get the owner and their email for account 123.

This last one would return something like:

```sql
SELECT
  accounts.owner_id,
  user.user_email
FROM
  accounts
JOIN user
  ON accounts.owner_id = user.id
WHERE
  accounts.key = 123;
```

### Table File Path

`trinols` has support for table information autocomplete. You will need to provide a
tables.json file containing the information needed. Change this setting in trinols settings
in vscode (`>> preferences > settings > trinols: Tables File Path`)

An example of tables.json would be:

- `signature` is used as a heading
- `name` is used for the value used to autocomplete
- `description` is the big description. This supports markdown.

```json
{
  "tables": [
    {
      "name": "orders",
      "description": "Table provides all order for the accounts.",
      "signature": "orders ",
      "columns": [
        {
          "name": "order_number",
          "signature": "order_number | number",
          "description": "The order number"
        }, 
        {
          "name": "account_name",
          "signature": "account_name | string",
          "description": "The order account name"
        }
      ]
    }
  ]
}
```

There is also a `trinols.setTrinolsTablesFilePath` that you can use in your extensions
if you want to pass in the path for each workspace dynamically. Trinols will keep track
of the workspace folder and trinols table path combinations. This allows for multiple
table files per workspace.

```typescript
const path = "/somePlaceInYourComputer/tables.json"
vscode.commands.executeCommand("trinols.setTrinolsTablesFilePath", path);
```

## Updating Docs 

The commands below will update the trinols resources. This process
gather data from trino's website and format them. 

```shell
poetry install
poetry run trinols update
```

You may need to update the chrome driver. You can do this by running:

```shell
poetry add chromedriver-binary==129.0.6668.70
```
Update the chrone version.

## Publishing extension

Install vsce: 

```shell
npm install -g vsce
```

Login to vsce with your publisher: 

```shell
vsce login <publisher-name>
```

Then package and publish it:

```shell
$ vsce package
$ vsce publish
```

You can manage extensions here: https://marketplace.visualstudio.com/manage/publishers
